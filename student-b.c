#include "student-b.h"

int isOdd(int digit)
{
    return digit%2 == 1;
}


int min(int digits[], int size)
{
    int mini=digits[0];
    for (int i = 1; i < size; i++)
    {
        if (digits[i] < mini)
        {
            mini=digits[i];
        }

    }
    return mini;
}
