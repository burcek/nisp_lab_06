TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.c \
        student-a.c \
        student-b.c \
        student-c.c \
        student-d.c

HEADERS += \
    student-a.h \
    student-b.h \
    student-c.c \
    student-d.h

