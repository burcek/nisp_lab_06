#include <stdio.h>
#include "student-b.h"
#include "student-a.h"
#include "student-c.h"
#include "student-d.h"

// student A - Bartosz Burzec
// student B - Radoslaw Barwacz
// student C - Mateusz Ciszek
// student D - Krzysztof Bogusz

int main()
{
    int Tab[6] = {2, 1, 3, 7, 4, 6};
    int a = 5;
    int b = isOdd(a);
    int c = isEven(a);
    int d = min(Tab, 6);
    int e = max(Tab, 6);
    int f = average(Tab,6);
    int g = isPrime(a);
    int h = isPositive(a);
    int i = countEven(Tab, 6);

    printf("Czy liczba %d jest nieparzysta?: %d \n", a, b);
    printf("Czy liczba %d jest parzysta?: %d", a, c);
    printf("Wartosc minimalna w tablicy wynosi %d \n", d);
    printf("Wartosc maksymalna w tablicy wynosi %d \n", e);
    printf("Srednia Arytmetyczna wartosci w tablicy wynosi %d \n", f);
    printf("Czy liczba %d jest liczba pierwsza?: %d \n", a, g);
    printf("Czy podana wartosc %d jest liczba dodatnia: %d \n", a, h);
    printf("Ilosc liczb parzystych: %d \n", i);

    return 0;
}

