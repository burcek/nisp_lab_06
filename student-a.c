#include "student-a.h"

int isEven(int digit)
{
    return digit % 2 == 0;
}

int max(int digits[], int size)
{
    int maxi = digits[0];
    for(int i = 1; i < size; i++)
    {
        if(digits[i] > maxi)
            maxi = digits[i];
    }
    return maxi;
}
