#include "student-d.h"

int isPositive(int digit) //Czy podana wartość jest liczbą dodatnią
{
    if(digit>0) return 1;
    else return 0;
}

int countEven(int digits [], int size) //Zliczanie liczb parzystych
{
    int counter=0;

    for(int i=0; i<size; i++)
    {
        if(digits[i]%2==0) counter++;
    }

    return counter;
}
